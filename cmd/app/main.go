package main

import "gitlab.com/ABDURAZZAKK/mailing_go/internal/app"

const configPath = "config/config.yaml"

func main() {
	app.Run(configPath)
}
