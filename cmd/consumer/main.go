package main

import (
	log "github.com/sirupsen/logrus"

	"gitlab.com/ABDURAZZAKK/mailing_go/cmd/consumer/tasks"
	"gitlab.com/ABDURAZZAKK/mailing_go/config"
	"gitlab.com/ABDURAZZAKK/mailing_go/internal/app"
	"gitlab.com/ABDURAZZAKK/mailing_go/pkg/broker"
	"gitlab.com/ABDURAZZAKK/mailing_go/pkg/postgres"
)

func main() {

	cfg, err := config.NewConfig("config/config.yaml")
	if err != nil {
		log.Fatalf("consumer main - config.NewConfig: %s", err)
	}

	// Logger
	app.SetLogrus(cfg.Log.Level, cfg.TIME.FORMAT)

	rabbit, err := broker.NewRabbitMQ(cfg.BROKER.URL)
	// rabbit, err := broker.NewRabbitMQ("amqp://guest:guest@localhost:5672")
	if err != nil {
		log.Fatalf("consumer main - broker.NewRabbitMQ: %v", err)
	}
	pg, err := postgres.New(cfg.PG.URL, postgres.MaxPoolSize(cfg.PG.MaxPoolSize))
	// pg, err := postgres.New("postgres://postgres:postgres@localhost:5433/postgres", postgres.MaxPoolSize(20))
	if err != nil {
		log.Fatalf("consumer main - postgres.New: %v", err)
	}
	messages, err := rabbit.Channel.Consume(
		rabbit.Queue.Name, // queue
		"",                // consumer
		true,              // auto-ack
		false,             // exclusive
		false,             // no-local
		false,             // no-wait
		nil,               // args
	)
	if err != nil {
		log.Fatalf("consumer main - rabbit.Channel.Consume: %v", err)
	}

	var forever chan struct{}

	go func() {
		var tokens = make(chan struct{}, 10)
		for message := range messages {
			msg, err := broker.MsgDeserialize(message.Body)
			if err != nil {
				log.Errorf("consumer main - broker.MsgDeserialize: %v", err)
			}
			switch msg["task"] {
			case "CreateMailing":
				run(tokens, func() { tasks.RunMailing(pg, msg) })
			case "StatisticToEmailEveryday":
				run(tokens, func() { tasks.CallEveryday(func() { tasks.SendStatisticToEmail(pg) }) })
			}

		}
	}()

	log.Info(" [*] Waiting for messages. To exit press CTRL+C")
	<-forever
}

func run(tokens chan struct{}, f func()) {
	tokens <- struct{}{}
	go f()
	<-tokens
}
