package tasks

import (
	"context"
	"fmt"

	log "github.com/sirupsen/logrus"
	"gitlab.com/ABDURAZZAKK/mailing_go/config"
	"gitlab.com/ABDURAZZAKK/mailing_go/internal/entity"
	"gitlab.com/ABDURAZZAKK/mailing_go/internal/repo/pgdb"
	"gitlab.com/ABDURAZZAKK/mailing_go/pkg/broker"
	"gitlab.com/ABDURAZZAKK/mailing_go/pkg/postgres"

	"time"
	_ "time/tzdata"
)

var LOCATION, _ = time.LoadLocation("Europe/Moscow")

type MailingMsg struct {
	Id           int
	Message      string
	StartAt      time.Time
	StopAt       time.Time
	ClientFilter entity.ClientFilter
}

func parseMoscowTime(t string) (time.Time, error) {
	cfg, err := config.NewConfig("config/config.yaml")
	if err != nil {
		log.Fatalf("parseMoscowTime - config.NewConfig: %v", err)
	}
	return time.ParseInLocation(cfg.TIME.FORMAT, t, LOCATION)

}

func MoscowNow() time.Time {

	return time.Now().In(LOCATION)
}

func callAt(callTime time.Time, f func()) {
	duration := callTime.Sub(MoscowNow())
	time.Sleep(duration)
	f()
}

func CallEveryday(f func()) {
	for {
		go f()
		duration := MoscowNow().Add(24 * time.Hour).Sub(MoscowNow())
		time.Sleep(duration)
	}
}

func parseMailingMsg(pg *postgres.Postgres, msg broker.Message) (MailingMsg, error) {
	start_at, err := parseMoscowTime(msg["start_at"].(string))
	if err != nil {
		return MailingMsg{}, fmt.Errorf("ParseMeilingMsg parseMoscowTime start: %v", err)
	}
	stop_at, err := parseMoscowTime(msg["stop_at"].(string))
	if err != nil {
		return MailingMsg{}, fmt.Errorf("ParseMeilingMsg parseMoscowTime stop: %v", err)
	}

	cFilter, err := pgdb.NewMailingRepo(pg).GetFilterById(
		context.Background(),
		int(msg["client_filter"].(float64)))
	if err != nil {
		return MailingMsg{}, fmt.Errorf("ParseMeilingMsg GetFilterById: %v", err)
	}
	mailingMsg := MailingMsg{
		int(msg["id"].(float64)),
		msg["message"].(string),
		start_at,
		stop_at,
		cFilter,
	}
	return mailingMsg, nil
}
