package tasks

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	log "github.com/sirupsen/logrus"
	"gitlab.com/ABDURAZZAKK/mailing_go/internal/repo/pgdb"
	"gitlab.com/ABDURAZZAKK/mailing_go/pkg/postgres"
)

func TodaysMessagesJSON(pg *postgres.Postgres) ([]byte, error) {
	messages, err := pgdb.NewMessageRepo(pg).GetSentToday(context.TODO())
	if err != nil {
		return nil, fmt.Errorf("TodaysMessages - pgdb.NewMessageRepo(pg).GetSentToday: %v", err)
	}
	type response struct {
		Id        int       `json:"id"`
		Status    string    `json:"status"`
		CreatedAt time.Time `json:"created_at"`
		ClientId  int       `json:"client_id"`
		MailingId int       `json:"mailing_id"`
	}
	var resp []response
	for _, msg := range messages {
		resp = append(resp, response{
			Id:        msg.Id,
			Status:    msg.Status,
			CreatedAt: msg.CreatedAt,
			ClientId:  msg.ClientId,
			MailingId: msg.MailingId,
		})
	}
	jd, err := json.Marshal(resp)
	if err != nil {
		return nil, fmt.Errorf("TodaysMessages - json.Marshal: %v", err)
	}
	return jd, nil
}

func SendStatisticToEmail(pg *postgres.Postgres) {
	sender := NewSender()
	m := NewMessage("Todays Statistic", " ")

	m.To = []string{TO}
	bfile, err := TodaysMessagesJSON(pg)
	if err != nil {
		log.Errorf("SendStatisticToEmail - TodaysMessagesJSON: %v", err)
		return
	}
	m.AttachFileWithBytes(bfile)

	log.Infof("SendStatisticToEmail - sender.Send: %v", sender.Send(m))
}
