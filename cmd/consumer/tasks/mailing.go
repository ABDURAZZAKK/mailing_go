package tasks

import (
	"bytes"
	"context"
	"os"

	"fmt"
	"net/http"

	"gitlab.com/ABDURAZZAKK/mailing_go/internal/entity"
	"gitlab.com/ABDURAZZAKK/mailing_go/internal/repo/pgdb"
	"gitlab.com/ABDURAZZAKK/mailing_go/pkg/broker"
	"gitlab.com/ABDURAZZAKK/mailing_go/pkg/postgres"

	log "github.com/sirupsen/logrus"
)

const API_URL = "https://probe.fbrq.cloud/v1"

func RunMailing(pg *postgres.Postgres, msg broker.Message) {
	mMsg, err := parseMailingMsg(pg, msg)
	if err != nil {
		log.Errorf("RunMailing - parseMailingMsg: %v", err)
		return
	}

	if MoscowNow().Before(mMsg.StartAt) {
		log.Infof("RunMailing - Mailing runing at %v", mMsg.StartAt)
		callAt(mMsg.StartAt, func() { Mailing(pg, mMsg) })
	} else if MoscowNow().Before(mMsg.StopAt) {
		Mailing(pg, mMsg)
	}

}

func Mailing(pg *postgres.Postgres, mMsg MailingMsg) {
	var tokens = make(chan struct{}, 10)

	clients, err := pgdb.NewClientRepo(pg).GetByClientFilter(context.TODO(), mMsg.ClientFilter)
	if err != nil {
		log.Errorf("Mailing - pgdb.NewClientRepo(pg).GetByClientFilter: %v", err)
	}
	for _, client := range clients {
		if MoscowNow().Before(mMsg.StopAt) {
			tokens <- struct{}{}
			go sendMsg(pg, client, mMsg)
			<-tokens
		} else {
			break
		}
	}
}

func sendMsg(pg *postgres.Postgres, client entity.Client, mMsg MailingMsg) {
	// Open Transaction
	tx, err := pg.Pool.Begin(context.TODO())
	if err != nil {
		log.Errorf("sendMsg - pg.Pool.Begin: %v", err)
		return
	}

	// Close Transaction
	defer func() {
		if err != nil {
			tx.Rollback(context.TODO())
		} else {
			tx.Commit(context.TODO())
		}
	}()

	// Create new Message
	sql, args, _ := pg.Builder.
		Insert("message").
		Columns("status", "created_at", "client_id", "mailing_id").
		Values(entity.MSG_STATUS_SENT, MoscowNow(), client.Id, mMsg.Id).
		Suffix("RETURNING id").
		ToSql()

	var msgId int
	if err = tx.QueryRow(context.TODO(), sql, args...).Scan(&msgId); err != nil {
		log.Errorf("sendMsg - tx.QueryRow: %v", err)
		return
	}

	// Sending a message to API
	jsonData := []byte(fmt.Sprintf(`{
		"id": %d,
		"phone": %v,
		"text": "%v"
	}`, msgId, client.Phone, mMsg.Message))

	url := fmt.Sprintf(API_URL+"/send/%d", msgId)

	req, err := http.NewRequest("POST", url, bytes.NewBuffer(jsonData))
	if err != nil {
		log.Errorf("sendMsg - http.NewRequest: %v", err)
		return
	}
	req.Header.Set("Authorization", "Bearer "+os.Getenv("API_MAILING_TOKEN"))
	req.Header.Set("Content-Type", "application/json")

	http_client := &http.Client{}
	resp, err := http_client.Do(req)
	if err != nil {
		log.Errorf("sendMsg - http_client.Do: %v", err)
		return
	}
	defer resp.Body.Close()

	// Update Message.Status
	msgStatus := entity.MSG_STATUS_DELIVERED
	if resp.StatusCode != 200 {
		msgStatus = fmt.Sprintf(entity.MSG_STATUS_ERROR, resp.Status)

	}
	log.Infof("msgStatus - %v", msgStatus)
	sql, args, _ = pg.Builder.
		Update("message").
		Set("status", msgStatus).
		Where("id = ?", msgId).
		ToSql()

	_, err = tx.Exec(context.TODO(), sql, args...)
	if err != nil {
		log.Errorf("sendMsg - tx.Exec: %v", err)
		return
	}

}
