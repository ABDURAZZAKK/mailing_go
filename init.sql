-- Active: 1693056748447@@127.0.0.1@5433@postgres
CREATE TABLE client (
    id            SERIAL PRIMARY KEY,
    phone         VARCHAR(12) UNIQUE NOT NULL,
    operator_code VARCHAR(3),
    tag           VARCHAR(100),
    timezone      VARCHAR(20)
);

CREATE TABLE client_filter (
    id            SERIAL PRIMARY KEY, 
    operator_code VARCHAR(3),
    tag           VARCHAR(100)
);

CREATE TABLE client_mailing (
    id            SERIAL PRIMARY KEY,
    message       VARCHAR(500) NOT NULL,
    start_at      TIMESTAMP NOT NULL,
    stop_at       TIMESTAMP NOT NULL, 
    client_filter INT REFERENCES client_filter(id) on delete set null
); 

CREATE TABLE message (
    id         SERIAL PRIMARY KEY,
    status     VARCHAR(50) NOT NULL,
    created_at TIMESTAMP NOT NULL,
    client_id  INT REFERENCES client(id) on delete set null,
    mailing_id INT REFERENCES client_mailing(id) on delete set null
);