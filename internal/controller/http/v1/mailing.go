package v1

import (
	"net/http"
	"strconv"
	"time"

	"github.com/labstack/echo/v4"
	log "github.com/sirupsen/logrus"
	"gitlab.com/ABDURAZZAKK/mailing_go/internal/entity"
	"gitlab.com/ABDURAZZAKK/mailing_go/internal/service"
)

type mailingRoutes struct {
	mailingService service.Mailing
}

func newMailingRoutes(g *echo.Group, mailingService service.Mailing) {
	r := &mailingRoutes{
		mailingService: mailingService,
	}
	g.GET("/statistics/general", r.generalStatistics)
	g.GET("/statistics/:id", r.getMessagesByMailingId)
	g.POST("", r.create)
	g.POST("/filter", r.createFilter)
	g.PUT("", r.update)
	g.DELETE("", r.delete)
}

// @Summary General Statistics
// @Description General Statistics
// @Tags Mailing
// @Accept json
// @Produce json
// @Success 200 {object} v1.mailingRoutes.generalStatistics.response
// @Failure 500 {object} echo.HTTPError
// @Router /api/v1/mailing/statistics/general [get]
func (r *mailingRoutes) generalStatistics(c echo.Context) error {
	GSs, err := r.mailingService.GeneralMessageStatistics(c.Request().Context())
	if err != nil {
		newErrorResponse(c, http.StatusInternalServerError, "internal server error")
		return err
	}

	type response struct {
		MailingId    int    `json:"mailing_id"`
		Status       string `json:"status"`
		MessageCount int    `json:"message_count"`
	}
	var resp []response
	for _, gs := range GSs {
		resp = append(resp, response{
			MailingId:    gs.MailingId,
			Status:       gs.Status,
			MessageCount: gs.MessageCount,
		})
	}

	return c.JSON(http.StatusOK, resp)
}

// @Summary Mailing Statistics
// @Description Mailing Statistics
// @Tags Mailing
// @Accept json
// @Produce json
// @Param id path int true "ID"
// @Success 200 {object} v1.mailingRoutes.getMessagesByMailingId.response
// @Failure 500 {object} echo.HTTPError
// @Router /api/v1/mailing/statistics/{id} [get]
func (r *mailingRoutes) getMessagesByMailingId(c echo.Context) error {
	mId, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		newErrorResponse(c, http.StatusBadRequest, "invalid request param")
		return err
	}

	messages, err := r.mailingService.GetMessagesByMailingId(c.Request().Context(), mId)
	if err != nil {
		newErrorResponse(c, http.StatusInternalServerError, "internal server error")
		return err
	}

	type response struct {
		Id        int       `json:"id"`
		Status    string    `json:"status"`
		CreatedAt time.Time `json:"created_at"`
		ClientId  int       `json:"client_id"`
		MailingId int       `json:"mailing_id"`
	}
	var resp []response
	for _, msg := range messages {
		resp = append(resp, response{
			Id:        msg.Id,
			Status:    msg.Status,
			CreatedAt: msg.CreatedAt,
			ClientId:  msg.ClientId,
			MailingId: msg.MailingId,
		})
	}

	return c.JSON(http.StatusOK, resp)
}

// @Summary Create Mailing
// @Description Create Mailing
// @Tags Mailing
// @Accept json
// @Produce json
// @Param input body entity.MailingIn true "input"
// @Success 200 {object} v1.mailingRoutes.create.response
// @Failure 500 {object} echo.HTTPError
// @Router /api/v1/mailing [post]
func (r *mailingRoutes) create(c echo.Context) error {
	var input entity.MailingIn
	if err := c.Bind(&input); err != nil {
		newErrorResponse(c, http.StatusBadRequest, "invalid request body")
		return err
	}
	if err := c.Validate(input); err != nil {
		newErrorResponse(c, http.StatusBadRequest, err.Error())
		return err
	}

	mailing, err := r.mailingService.Create(c.Request().Context(), input)
	if err != nil {
		newErrorResponse(c, http.StatusInternalServerError, "internal server error")
		return err
	}
	log.Infof("Mailing %d created", mailing.Id)

	type response struct {
		Id           int       `json:"id"`
		Message      string    `json:"message"`
		StartAt      time.Time `json:"start_at"`
		StopAt       time.Time `json:"stop_at"`
		ClientFilter int       `json:"client_filter"`
	}
	return c.JSON(http.StatusCreated, response{
		Id:           mailing.Id,
		Message:      mailing.Message,
		StartAt:      mailing.StartAt,
		StopAt:       mailing.StopAt,
		ClientFilter: mailing.ClientFilter,
	})
}

// @Summary Create ClientFilter
// @Description Create ClientFilter for mailing
// @Tags Mailing
// @Accept json
// @Produce json
// @Param input body entity.ClientFilterIn true "input"
// @Success 200 {object} v1.mailingRoutes.createFilter.response
// @Failure 500 {object} echo.HTTPError
// @Router /api/v1/mailing/filter [post]
func (r *mailingRoutes) createFilter(c echo.Context) error {
	var input entity.ClientFilter
	if err := c.Bind(&input); err != nil {
		newErrorResponse(c, http.StatusBadRequest, "invalid request body")
		return err
	}
	if err := c.Validate(input); err != nil {
		newErrorResponse(c, http.StatusBadRequest, err.Error())
		return err
	}
	id, err := r.mailingService.CreateFilter(c.Request().Context(), input)
	if err != nil {
		newErrorResponse(c, http.StatusInternalServerError, "internal server error")
		return err
	}
	log.Infof("ClientFilter %d created", id)

	type response struct {
		Id int `json:"id"`
	}
	return c.JSON(http.StatusCreated, response{
		Id: id,
	})
}

// @Summary Update Mailing
// @Description Update Mailing
// @Tags Mailing
// @Accept json
// @Produce json
// @Param input body entity.Mailing true "input"
// @Success 200 {object} v1.mailingRoutes.update.response
// @Failure 500 {object} echo.HTTPError
// @Router /api/v1/mailing [put]
func (r *mailingRoutes) update(c echo.Context) error {
	var input entity.Mailing
	if err := c.Bind(&input); err != nil {
		newErrorResponse(c, http.StatusBadRequest, "invalid request body")
		return err
	}
	if err := c.Validate(input); err != nil {
		newErrorResponse(c, http.StatusBadRequest, err.Error())
		return err
	}
	id, err := r.mailingService.Update(c.Request().Context(), input)
	if err != nil {
		newErrorResponse(c, http.StatusInternalServerError, "internal server error")
		return err
	}
	log.Infof("Mailing %d updated", id)

	type response struct {
		Id int `json:"id"`
	}
	return c.JSON(http.StatusOK, response{
		Id: id,
	})
}

type mailingDeleteInput struct {
	Id int `json:"id"`
}

// @Summary Delete Mailing
// @Description Delete Mailing
// @Tags Mailing
// @Accept json
// @Produce json
// @Param input body mailingDeleteInput true "input"
// @Success 200 {object} v1.mailingRoutes.delete.response
// @Failure 500 {object} echo.HTTPError
// @Router /api/v1/mailing [delete]
func (r *mailingRoutes) delete(c echo.Context) error {
	var input mailingDeleteInput
	if err := c.Bind(&input); err != nil {
		newErrorResponse(c, http.StatusBadRequest, "invalid request body")
		return err
	}
	if err := c.Validate(input); err != nil {
		newErrorResponse(c, http.StatusBadRequest, err.Error())
		return err
	}
	err := r.mailingService.Delete(c.Request().Context(), input.Id)
	if err != nil {
		newErrorResponse(c, http.StatusInternalServerError, "internal server error")
		return err
	}
	log.Infof("Mailing %d deleted", input.Id)

	return c.NoContent(http.StatusOK)
}
