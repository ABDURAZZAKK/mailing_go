package v1

import (
	"net/http"

	"github.com/labstack/echo/v4"
	log "github.com/sirupsen/logrus"
	"gitlab.com/ABDURAZZAKK/mailing_go/internal/entity"
	"gitlab.com/ABDURAZZAKK/mailing_go/internal/service"
)

type clientRoutes struct {
	clientService service.Client
}

func newClientRoutes(g *echo.Group, clientService service.Client) {
	r := &clientRoutes{
		clientService: clientService,
	}
	g.POST("", r.create)
	g.PUT("", r.update)
	g.DELETE("", r.delete)
}

// @Summary Create Client
// @Description Create Client
// @Tags Client
// @Accept json
// @Produce json
// @Param input body entity.ClientIn true "input"
// @Success 201 {object} v1.clientRoutes.create.response
// @Failure 500 {object} echo.HTTPError
// @Router /api/v1/client [post]
func (r *clientRoutes) create(c echo.Context) error {
	var input entity.Client
	if err := c.Bind(&input); err != nil {
		newErrorResponse(c, http.StatusBadRequest, "invalid request body")
		return err
	}
	if err := c.Validate(input); err != nil {
		newErrorResponse(c, http.StatusBadRequest, err.Error())
		return err
	}
	id, err := r.clientService.Create(c.Request().Context(), input)
	if err != nil {
		newErrorResponse(c, http.StatusInternalServerError, "internal server error")
		return err
	}
	log.Infof("Client %d created", id)
	type response struct {
		Id int `json:"id"`
	}
	return c.JSON(http.StatusCreated, response{
		Id: id,
	})
}

// @Summary Update Client
// @Description Update Client
// @Tags Client
// @Accept json
// @Produce json
// @Param input body entity.Client true "input"
// @Success 200 {object} v1.clientRoutes.update.response
// @Failure 500 {object} echo.HTTPError
// @Router /api/v1/client [put]
func (r *clientRoutes) update(c echo.Context) error {
	var input entity.Client
	if err := c.Bind(&input); err != nil {
		newErrorResponse(c, http.StatusBadRequest, "invalid request body")
		return err
	}
	if err := c.Validate(input); err != nil {
		newErrorResponse(c, http.StatusBadRequest, err.Error())
		return err
	}
	id, err := r.clientService.Update(c.Request().Context(), input)
	if err != nil {
		newErrorResponse(c, http.StatusInternalServerError, "internal server error")
		return err
	}
	log.Infof("Client %d updated", id)

	type response struct {
		Id int `json:"id"`
	}
	return c.JSON(http.StatusOK, response{
		Id: id,
	})
}

type clientDeleteInput struct {
	Id int `json:"id"`
}

// @Summary Delete Client
// @Description Delete Client
// @Tags Client
// @Accept json
// @Produce json
// @Param input body clientDeleteInput true "input"
// @Success 200 {object} v1.clientRoutes.delete.response
// @Failure 500 {object} echo.HTTPError
// @Router /api/v1/client [delete]
func (r *clientRoutes) delete(c echo.Context) error {
	var input clientDeleteInput
	if err := c.Bind(&input); err != nil {
		newErrorResponse(c, http.StatusBadRequest, "invalid request body")
		return err
	}
	if err := c.Validate(input); err != nil {
		newErrorResponse(c, http.StatusBadRequest, err.Error())
		return err
	}
	err := r.clientService.Delete(c.Request().Context(), input.Id)
	if err != nil {
		newErrorResponse(c, http.StatusInternalServerError, "internal server error")
		return err
	}
	log.Infof("Client %d deleted", input.Id)

	return c.NoContent(http.StatusOK)
}
