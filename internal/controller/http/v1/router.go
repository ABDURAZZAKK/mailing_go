package v1

import (
	"os"

	"gitlab.com/ABDURAZZAKK/mailing_go/internal/service"

	log "github.com/sirupsen/logrus"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"

	echoSwagger "github.com/swaggo/echo-swagger"
	_ "gitlab.com/ABDURAZZAKK/mailing_go/docs"
)

func NewRouter(handler *echo.Echo, services *service.Services) {
	handler.Use(middleware.LoggerWithConfig(middleware.LoggerConfig{
		Format: `{"time":"${time_rfc3339_nano}", "method":"${method}","uri":"${uri}", "status":${status},"error":"${error}"}` + "\n",
		Output: setLogsFile(),
	}))
	handler.Use(middleware.Recover())
	handler.GET("/ping", func(c echo.Context) error { return c.NoContent(200) })
	handler.GET("/docs/*", echoSwagger.WrapHandler)
	v1 := handler.Group("/api/v1")
	{
		newClientRoutes(v1.Group("/client"), services.Client)
		newMailingRoutes(v1.Group("/mailing"), services.Mailing)
	}
}
func setLogsFile() *os.File {
	file, err := os.OpenFile("./logs/requests.log", os.O_APPEND|os.O_CREATE|os.O_RDWR, 0666)
	if err != nil {
		log.Fatal(err)
	}
	return file
}
