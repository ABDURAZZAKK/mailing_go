package pgdb

import (
	"context"
	"fmt"

	// log "github.com/sirupsen/logrus"
	"gitlab.com/ABDURAZZAKK/mailing_go/internal/entity"
	"gitlab.com/ABDURAZZAKK/mailing_go/pkg/postgres"
)

type MessageRepo struct {
	*postgres.Postgres
}

func NewMessageRepo(pg *postgres.Postgres) *MessageRepo {
	return &MessageRepo{pg}
}

func (r *MessageRepo) GeneralStatistics(ctx context.Context) ([]entity.GeneralStatistics, error) {
	sql := `SELECT mailing_id, status, COUNT(*) as num_messages 
			FROM message 
			GROUP BY mailing_id, status
			ORDER BY mailing_id DESC;
			`

	rows, err := r.Pool.Query(ctx, sql)
	if err != nil {
		return nil, fmt.Errorf("MessageRepo.GeneralStatistics - r.Pool.Query: %v", err)
	}
	defer rows.Close()
	var GSs []entity.GeneralStatistics
	for rows.Next() {
		var gs entity.GeneralStatistics
		err := rows.Scan(
			&gs.MailingId,
			&gs.Status,
			&gs.MessageCount,
		)
		if err != nil {
			return nil, fmt.Errorf("MessageRepo.GeneralStatistics - rows.Scan: %v", err)
		}
		GSs = append(GSs, gs)
	}

	if err = rows.Err(); err != nil {
		return nil, fmt.Errorf("MessageRepo.GeneralStatistics - rows.Err: %v", err)
	}

	return GSs, nil
}

func (r *MessageRepo) GetAll(ctx context.Context) ([]entity.Message, error) {
	sql := "SELECT * FROM message"

	rows, err := r.Pool.Query(ctx, sql)
	if err != nil {
		return nil, fmt.Errorf("MessageRepo.GetAll - r.Pool.Query: %v", err)
	}
	defer rows.Close()
	var messages []entity.Message
	for rows.Next() {
		var message entity.Message
		err := rows.Scan(
			&message.Id,
			&message.Status,
			&message.CreatedAt,
			&message.ClientId,
			&message.MailingId,
		)
		if err != nil {
			return nil, fmt.Errorf("MessageRepo.GetAll - rows.Scan: %v", err)
		}
		messages = append(messages, message)
	}

	if err = rows.Err(); err != nil {
		return nil, fmt.Errorf("MessageRepo.GetAll - rows.Err: %v", err)
	}

	return messages, nil

}

func (r *MessageRepo) GetByMailingId(ctx context.Context, mailingId int) ([]entity.Message, error) {
	sql, args, _ := r.Builder.
		Select("*").
		From("message").
		Where("mailing_id = ?", mailingId).
		ToSql()

	rows, err := r.Pool.Query(ctx, sql, args...)
	if err != nil {
		return nil, fmt.Errorf("MessageRepo.GetByMailingId - r.Pool.Query: %v", err)
	}
	defer rows.Close()
	var messages []entity.Message
	for rows.Next() {
		var message entity.Message
		err := rows.Scan(
			&message.Id,
			&message.Status,
			&message.CreatedAt,
			&message.ClientId,
			&message.MailingId,
		)
		if err != nil {
			return nil, fmt.Errorf("MessageRepo.GetByMailingId - rows.Scan: %v", err)
		}
		messages = append(messages, message)
	}

	if err = rows.Err(); err != nil {
		return nil, fmt.Errorf("MessageRepo.GetByMailingId - rows.Err: %v", err)
	}

	return messages, nil
}

func (r *MessageRepo) GetSentToday(ctx context.Context) ([]entity.Message, error) {
	sql, args, _ := r.Builder.
		Select("*").
		From("message").
		Where("created_at > now() - interval '24 hour'").
		ToSql()

	rows, err := r.Pool.Query(ctx, sql, args...)
	if err != nil {
		return nil, fmt.Errorf("MessageRepo.GetSentToday - r.Pool.Query: %v", err)
	}
	defer rows.Close()
	var messages []entity.Message
	for rows.Next() {
		var message entity.Message
		err := rows.Scan(
			&message.Id,
			&message.Status,
			&message.CreatedAt,
			&message.ClientId,
			&message.MailingId,
		)
		if err != nil {
			return nil, fmt.Errorf("MessageRepo.GetSentToday - rows.Scan: %v", err)
		}
		messages = append(messages, message)
	}

	if err = rows.Err(); err != nil {
		return nil, fmt.Errorf("MessageRepo.GetSentToday - rows.Err: %v", err)
	}

	return messages, nil
}
