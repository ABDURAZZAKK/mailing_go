package pgdb

import (
	"context"
	"errors"
	// "errors"
	"fmt"

	"gitlab.com/ABDURAZZAKK/mailing_go/internal/entity"
	"gitlab.com/ABDURAZZAKK/mailing_go/pkg/postgres"

	sq "github.com/Masterminds/squirrel"
	"github.com/jackc/pgx/v5/pgconn"
	log "github.com/sirupsen/logrus"
)

type ClientRepo struct {
	*postgres.Postgres
}

func NewClientRepo(pg *postgres.Postgres) *ClientRepo {
	return &ClientRepo{pg}
}

func (r *ClientRepo) GetById(ctx context.Context, id int) (entity.Client, error) {
	sql, args, _ := r.Builder.
		Select("*").
		From("client").
		Where("id = ?", id).
		ToSql()

	var client entity.Client
	err := r.Pool.QueryRow(ctx, sql, args...).Scan(
		&client.Id,
		&client.Phone,
		&client.OperatorCode,
		&client.Tag,
		&client.Timezone,
	)
	if err != nil {
		return entity.Client{}, fmt.Errorf("ClientRepo.GetById - r.Pool.QueryRow: %v", err)
	}
	return client, nil
}

func (r *ClientRepo) GetByClientFilter(ctx context.Context, cFilter entity.ClientFilter) ([]entity.Client, error) {
	And := sq.And{}
	if cFilter.OperatorCode != "" {
		And = append(And, sq.Eq{"operator_code": cFilter.OperatorCode})
	}
	if cFilter.Tag != "" {
		And = append(And, sq.Eq{"tag": cFilter.Tag})
	}
	sql, args, _ := r.Builder.
		Select("*").
		From("client").
		Where(And).
		ToSql()

	rows, err := r.Pool.Query(ctx, sql, args...)
	if err != nil {
		return nil, fmt.Errorf("ClientRepo.GetByClientFilter - r.Pool.Query: %v", err)
	}
	defer rows.Close()
	var clients []entity.Client
	for rows.Next() {
		var client entity.Client
		err := rows.Scan(
			&client.Id,
			&client.Phone,
			&client.OperatorCode,
			&client.Tag,
			&client.Timezone,
		)
		if err != nil {
			return nil, fmt.Errorf("ClientRepo.GetByClientFilter - rows.Scan: %v", err)
		}
		clients = append(clients, client)
	}

	if err = rows.Err(); err != nil {
		return nil, fmt.Errorf("ClientRepo.GetByClientFilter - rows.Err: %v", err)
	}

	return clients, nil
}

func (r *ClientRepo) Create(ctx context.Context, client entity.Client) (int, error) {
	sql, args, _ := r.Builder.
		Insert("client").
		Columns("phone", "operator_code", "tag", "timezone").
		Values(client.Phone, client.OperatorCode, client.Tag, client.Timezone).
		Suffix("RETURNING id").
		ToSql()

	var id int
	err := r.Pool.QueryRow(ctx, sql, args...).Scan(&id)
	if err != nil {
		var pgErr *pgconn.PgError
		if ok := errors.As(err, &pgErr); ok {
			log.Debugf("err: %v", pgErr)
		}
		log.Debugf("err: %v", err)
		return 0, fmt.Errorf("ClientRepo.Create - r.Pool.QueryRow: %v", err)
	}
	return id, nil
}

func (r *ClientRepo) Update(ctx context.Context, client entity.Client) (int, error) {
	sql, args, _ := r.Builder.
		Update("client").
		Set("phone", client.Phone).
		Set("operator_code", client.OperatorCode).
		Set("tag", client.Tag).
		Set("timezone", client.Timezone).
		Where("id = ?", client.Id).
		Suffix("RETURNING id").
		ToSql()

	var id int
	err := r.Pool.QueryRow(ctx, sql, args...).Scan(&id)
	if err != nil {
		log.Debugf("err: %v", err)
		return 0, fmt.Errorf("ClientRepo.Update - r.Pool.QueryRow: %v", err)
	}
	return id, nil
}

func (r *ClientRepo) Delete(ctx context.Context, id int) error {
	sql, args, _ := r.Builder.
		Delete("client").
		Where("id = ?", id).
		Suffix("RETURNING id").
		ToSql()

	var c_id int
	err := r.Pool.QueryRow(ctx, sql, args...).Scan(&c_id)
	if err != nil {
		log.Debugf("err: %v", err)
		return fmt.Errorf("ClientRepo.Delete - r.Pool.QueryRow: %v", err)
	}
	return nil
}
