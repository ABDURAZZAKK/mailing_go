package pgdb

import (
	"context"
	"fmt"

	"gitlab.com/ABDURAZZAKK/mailing_go/internal/entity"
	"gitlab.com/ABDURAZZAKK/mailing_go/pkg/postgres"

	log "github.com/sirupsen/logrus"
)

type MailingRepo struct {
	*postgres.Postgres
}

func NewMailingRepo(pg *postgres.Postgres) *MailingRepo {
	return &MailingRepo{pg}
}

func (r *MailingRepo) GetById(ctx context.Context, id int) (entity.Mailing, error) {
	sql, args, _ := r.Builder.
		Select("*").
		From("client_mailing").
		Where("id = ?", id).
		ToSql()

	var mailing entity.Mailing
	err := r.Pool.QueryRow(ctx, sql, args...).Scan(
		&mailing.Id,
		&mailing.Message,
		&mailing.StartAt,
		&mailing.StopAt,
		&mailing.ClientFilter,
	)
	if err != nil {
		return entity.Mailing{}, fmt.Errorf("MailingRepo.GetById - r.Pool.QueryRow: %v", err)
	}
	return mailing, nil
}

func (r *MailingRepo) GetFilterById(ctx context.Context, id int) (entity.ClientFilter, error) {
	sql, args, _ := r.Builder.
		Select("*").
		From("client_filter").
		Where("id = ?", id).
		ToSql()

	var mailing entity.ClientFilter
	err := r.Pool.QueryRow(ctx, sql, args...).Scan(
		&mailing.Id,
		&mailing.OperatorCode,
		&mailing.Tag,
	)
	if err != nil {
		return entity.ClientFilter{}, fmt.Errorf("MailingRepo.GetFilterById - r.Pool.QueryRow: %v", err)
	}
	return mailing, nil
}

func (r *MailingRepo) Create(ctx context.Context, mailing entity.Mailing) (entity.Mailing, error) {
	sql, args, _ := r.Builder.
		Insert("client_mailing").
		Columns("message", "start_at", "stop_at", "client_filter").
		Values(mailing.Message, mailing.StartAt, mailing.StopAt, mailing.ClientFilter).
		Suffix("RETURNING *").
		ToSql()

	var m entity.Mailing
	err := r.Pool.QueryRow(ctx, sql, args...).Scan(
		&m.Id,
		&m.Message,
		&m.StartAt,
		&m.StopAt,
		&m.ClientFilter,
	)
	if err != nil {
		log.Debugf("err: %v", err)
		return entity.Mailing{}, fmt.Errorf("MailingRepo.Create - r.Pool.QueryRow: %v", err)
	}
	return m, nil
}

func (r *MailingRepo) CreateFilter(ctx context.Context, cFilter entity.ClientFilter) (int, error) {
	sql, args, _ := r.Builder.
		Insert("client_filter").
		Columns("operator_code", "tag").
		Values(cFilter.OperatorCode, cFilter.Tag).
		Suffix("RETURNING id").
		ToSql()

	var id int
	err := r.Pool.QueryRow(ctx, sql, args...).Scan(&id)
	if err != nil {
		log.Debugf("err: %v", err)
		return 0, fmt.Errorf("MailingRepo.CreateFilter - r.Pool.QueryRow: %v", err)
	}
	return id, nil
}

func (r *MailingRepo) Update(ctx context.Context, mailing entity.Mailing) (int, error) {
	sql, args, _ := r.Builder.
		Update("client_mailing").
		Set("message", mailing.Message).
		Set("start_at", mailing.StartAt).
		Set("stop_at", mailing.StopAt).
		Set("client_filter", mailing.ClientFilter).
		Where("id = ?", mailing.Id).
		Suffix("RETURNING id").
		ToSql()

	var id int
	err := r.Pool.QueryRow(ctx, sql, args...).Scan(&id)
	if err != nil {
		log.Debugf("err: %v", err)
		return 0, fmt.Errorf("MailingRepo.Update - r.Pool.QueryRow: %v", err)
	}
	return id, nil
}

func (r *MailingRepo) Delete(ctx context.Context, id int) error {
	sql, args, _ := r.Builder.
		Delete("client_mailing").
		Where("id = ?", id).
		Suffix("RETURNING id").
		ToSql()

	var m_id int
	err := r.Pool.QueryRow(ctx, sql, args...).Scan(&m_id)
	if err != nil {
		log.Debugf("err: %v", err)
		return fmt.Errorf("MailingRepo.Delete - r.Pool.QueryRow: %v", err)
	}
	return nil
}
