package repo

import (
	"context"

	"gitlab.com/ABDURAZZAKK/mailing_go/internal/entity"
	"gitlab.com/ABDURAZZAKK/mailing_go/internal/repo/pgdb"
	"gitlab.com/ABDURAZZAKK/mailing_go/pkg/postgres"
)

type Client interface {
	GetById(ctx context.Context, id int) (entity.Client, error)
	Create(ctx context.Context, client entity.Client) (int, error)
	Update(ctx context.Context, client entity.Client) (int, error)
	Delete(ctx context.Context, id int) error
}

type Mailing interface {
	GetById(ctx context.Context, id int) (entity.Mailing, error)
	GetFilterById(ctx context.Context, id int) (entity.ClientFilter, error)
	Create(ctx context.Context, mailing entity.Mailing) (entity.Mailing, error)
	CreateFilter(ctx context.Context, cFilter entity.ClientFilter) (int, error)
	Update(ctx context.Context, mailing entity.Mailing) (int, error)
	Delete(ctx context.Context, id int) error
}

type Message interface {
	GetAll(ctx context.Context) ([]entity.Message, error)
	GetByMailingId(ctx context.Context, mailingId int) ([]entity.Message, error)
	GeneralStatistics(ctx context.Context) ([]entity.GeneralStatistics, error)
}

type Repositories struct {
	Client
	Mailing
	Message
}

func NewRepositories(pg *postgres.Postgres) *Repositories {
	return &Repositories{
		Client:  pgdb.NewClientRepo(pg),
		Mailing: pgdb.NewMailingRepo(pg),
		Message: pgdb.NewMessageRepo(pg),
	}
}
