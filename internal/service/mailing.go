package service

import (
	"context"
	"fmt"
	"time"

	"gitlab.com/ABDURAZZAKK/mailing_go/config"
	"gitlab.com/ABDURAZZAKK/mailing_go/internal/entity"
	"gitlab.com/ABDURAZZAKK/mailing_go/internal/repo"
	"gitlab.com/ABDURAZZAKK/mailing_go/pkg/broker"

	log "github.com/sirupsen/logrus"
)

type MailingService struct {
	mailingRepo repo.Mailing
	messageRepo repo.Message
	rabbit      *broker.RabbitMQ
}

func NewMailingService(mailingRepo repo.Mailing, messageRepo repo.Message, rabbit *broker.RabbitMQ) *MailingService {
	return &MailingService{
		mailingRepo: mailingRepo,
		messageRepo: messageRepo,
		rabbit:      rabbit,
	}
}

func (s *MailingService) GetFilterById(ctx context.Context, id int) (entity.ClientFilter, error) {
	cFilter, err := s.mailingRepo.GetFilterById(ctx, id)
	if err != nil {
		return entity.ClientFilter{}, fmt.Errorf("MailingService.Create - mailingRepo.Create: %v", err)
	}
	return cFilter, nil
}

func (s *MailingService) Create(ctx context.Context, input entity.MailingIn) (entity.Mailing, error) {
	cfg, _ := config.NewConfig("config/config.yaml")

	startAt, err := time.Parse(cfg.TIME.FORMAT, input.StartAt)
	if err != nil {
		return entity.Mailing{}, fmt.Errorf("MailingService.Create - time.Parse start: %v", err)
	}
	stopAt, err := time.Parse(cfg.TIME.FORMAT, input.StopAt)
	if err != nil {
		return entity.Mailing{}, fmt.Errorf("MailingService.Create - time.Parse stop: %v", err)
	}

	m := entity.Mailing{
		Message:      input.Message,
		StartAt:      startAt,
		StopAt:       stopAt,
		ClientFilter: input.ClientFilter,
	}

	mailing, err := s.mailingRepo.Create(ctx, m)
	if err != nil {
		return entity.Mailing{}, fmt.Errorf("MailingService.Create - mailingRepo.Create: %v", err)
	}
	s.RunMailingTask(mailing)
	return mailing, nil
}

func (s *MailingService) CreateFilter(ctx context.Context, input entity.ClientFilter) (int, error) {
	id, err := s.mailingRepo.CreateFilter(ctx, input)
	if err != nil {
		return 0, fmt.Errorf("MailingService.CreateFilter - mailingRepo.CreateFilter: %v", err)
	}
	return id, nil
}

func (s *MailingService) Update(ctx context.Context, input entity.Mailing) (int, error) {
	id, err := s.mailingRepo.Update(ctx, input)
	if err != nil {
		return 0, fmt.Errorf("MailingService.Update - mailingRepo.Update: %v", err)
	}
	m, err := s.mailingRepo.GetById(ctx, id)
	if err != nil {
		log.Warnf("MailingService.Update - s.mailingRepo.GetById: %v", err)
	}
	s.RunMailingTask(m)
	return id, nil
}

func (s *MailingService) Delete(ctx context.Context, id int) error {
	err := s.mailingRepo.Delete(ctx, id)
	if err != nil {
		return fmt.Errorf("MailingService.Delete - mailingRepo.Delete: %v", err)
	}
	return nil
}

func (s *MailingService) RunMailingTask(mailing entity.Mailing) {
	msg, err := broker.MsgSerialize(broker.Message{
		"task":          "CreateMailing",
		"id":            mailing.Id,
		"message":       mailing.Message,
		"start_at":      mailing.StartAt,
		"stop_at":       mailing.StopAt,
		"client_filter": mailing.ClientFilter,
	})
	if err != nil {
		log.Warnf("MailingService.RunMailingTask - broker.MsgSerialize: %v", err)
	}

	err = s.rabbit.Publish(msg)
	if err != nil {
		log.Warnf("MailingService.RunMailingTask - s.rabbit.Publish: %v", err)
	}

}

func (s *MailingService) GeneralMessageStatistics(ctx context.Context) ([]entity.GeneralStatistics, error) {
	stats, err := s.messageRepo.GeneralStatistics(ctx)
	if err != nil {
		return nil, fmt.Errorf("MailingService.GeneralMessageStatistics - messageRepo.GeneralStatistics: %v", err)
	}
	return stats, nil
}

func (s *MailingService) GetMessagesByMailingId(ctx context.Context, mailingId int) ([]entity.Message, error) {
	messages, err := s.messageRepo.GetByMailingId(ctx, mailingId)
	if err != nil {
		return nil, fmt.Errorf("MailingService.GetMessagesByMailingId - messageRepo.GetByMailingId: %v", err)
	}
	return messages, nil
}
