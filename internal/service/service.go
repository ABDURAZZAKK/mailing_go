package service

import (
	"context"

	"gitlab.com/ABDURAZZAKK/mailing_go/internal/entity"
	"gitlab.com/ABDURAZZAKK/mailing_go/internal/repo"
	"gitlab.com/ABDURAZZAKK/mailing_go/pkg/broker"
)

type Client interface {
	Create(ctx context.Context, input entity.Client) (int, error)
	Update(ctx context.Context, input entity.Client) (int, error)
	Delete(ctx context.Context, id int) error
}

type Mailing interface {
	Create(ctx context.Context, input entity.MailingIn) (entity.Mailing, error)
	CreateFilter(ctx context.Context, input entity.ClientFilter) (int, error)
	Update(ctx context.Context, input entity.Mailing) (int, error)
	Delete(ctx context.Context, id int) error
	GeneralMessageStatistics(ctx context.Context) ([]entity.GeneralStatistics, error)
	GetMessagesByMailingId(ctx context.Context, mailingId int) ([]entity.Message, error)
}

type Services struct {
	Client
	Mailing
}

type ServicesDependencies struct {
	Repos  *repo.Repositories
	Rabbit *broker.RabbitMQ
}

func NewServices(deps ServicesDependencies) *Services {
	return &Services{
		Client:  NewClientService(deps.Repos.Client),
		Mailing: NewMailingService(deps.Repos.Mailing, deps.Repos.Message, deps.Rabbit),
	}
}
