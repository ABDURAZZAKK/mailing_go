package service

import (
	"context"
	"fmt"

	"gitlab.com/ABDURAZZAKK/mailing_go/internal/entity"
	"gitlab.com/ABDURAZZAKK/mailing_go/internal/repo"
)

type ClientService struct {
	clientRepo repo.Client
}

func NewClientService(clientRepo repo.Client) *ClientService {
	return &ClientService{clientRepo: clientRepo}
}

func (s *ClientService) Create(ctx context.Context, input entity.Client) (int, error) {
	id, err := s.clientRepo.Create(ctx, input)
	if err != nil {
		return 0, fmt.Errorf("ClientService.Create - clientRepo.Create: %v", err)
	}
	return id, nil
}

func (s *ClientService) Update(ctx context.Context, input entity.Client) (int, error) {
	id, err := s.clientRepo.Update(ctx, input)
	if err != nil {
		return 0, fmt.Errorf("ClientService.Update - clientRepo.Update: %v", err)
	}
	return id, nil
}

func (s *ClientService) Delete(ctx context.Context, id int) error {
	err := s.clientRepo.Delete(ctx, id)
	if err != nil {
		return fmt.Errorf("ClientService.Delete - clientRepo.Delete: %v", err)
	}
	return nil
}
