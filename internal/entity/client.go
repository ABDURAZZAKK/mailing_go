package entity

type Client struct {
	Id           int    `db:"id" json:"id"`
	Phone        string `db:"phone" json:"phone"`
	OperatorCode string `db:"operator_code" json:"operator_code"`
	Tag          string `db:"tag" json:"tag"`
	Timezone     string `db:"timezone" json:"timezone"`
}

type ClientIn struct {
	Phone        string `son:"phone"`
	OperatorCode string `json:"operator_code"`
	Tag          string `json:"tag"`
	Timezone     string `json:"timezone"`
}
