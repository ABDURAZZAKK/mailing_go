package entity

type ClientFilter struct {
	Id           int    `db:"id" json:"id"`
	OperatorCode string `db:"operator_code" json:"operator_code"`
	Tag          string `db:"tag" json:"tag"`
}

type ClientFilterIn struct {
	OperatorCode string `json:"operator_code"`
	Tag          string `json:"tag"`
}
