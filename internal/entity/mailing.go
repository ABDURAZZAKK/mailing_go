package entity

import "time"

type Mailing struct {
	Id           int       `db:"id" json:"id"`
	Message      string    `db:"message" json:"message"`
	StartAt      time.Time `db:"start_at" json:"start_at"`
	StopAt       time.Time `db:"stop_at" json:"stop_at"`
	ClientFilter int       `db:"client_filter" json:"client_filter"`
}

type MailingIn struct {
	Message      string `json:"message"`
	StartAt      string `json:"start_at"`
	StopAt       string `json:"stop_at"`
	ClientFilter int    `json:"client_filter"`
}
