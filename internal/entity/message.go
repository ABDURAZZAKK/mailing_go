package entity

import "time"

const (
	MSG_STATUS_SENT      = "Отправленно"
	MSG_STATUS_DELIVERED = "Доставленно"
	MSG_STATUS_ERROR     = "Ошибка: %v"
)

type Message struct {
	Id        int       `db:"id" json:"id"`
	Status    string    `db:"status" json:"status"`
	CreatedAt time.Time `db:"created_at" json:"created_at"`
	ClientId  int       `db:"client_id" json:"client_id"`
	MailingId int       `db:"mailing_id" json:"mailing_id"`
}

type GeneralStatistics struct {
	MailingId    int    `db:"mailing_id" json:"mailing_id"`
	Status       string `db:"status" json:"status"`
	MessageCount int    `db:"message_count" json:"message_count"`
}
