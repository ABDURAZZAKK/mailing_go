package app

import (
	log "github.com/sirupsen/logrus"
	"gitlab.com/ABDURAZZAKK/mailing_go/pkg/broker"
)

func startup(rabbit *broker.RabbitMQ) {
	sendStatisticToEmailEveryday(rabbit)
}

func sendStatisticToEmailEveryday(rabbit *broker.RabbitMQ) {
	msg, err := broker.MsgSerialize(broker.Message{
		"task": "StatisticToEmailEveryday",
	})
	if err != nil {
		log.Warnf("sendStatisticToEmailEveryday - broker.MsgSerialize: %v", err)
	}

	err = rabbit.Publish(msg)
	if err != nil {
		log.Warnf("sendStatisticToEmailEveryday - rabbit.Publish: %v", err)
	}
}
