package app

import (
	"github.com/sirupsen/logrus"
	"os"
)

func SetLogrus(level string, timeFormat string) {
	logrusLevel, err := logrus.ParseLevel(level)
	if err != nil {
		logrus.SetLevel(logrus.DebugLevel)
	} else {
		logrus.SetLevel(logrusLevel)
	}

	logrus.SetFormatter(&logrus.JSONFormatter{
		TimestampFormat: timeFormat,
	})

	logrus.SetOutput(os.Stdout)
}
